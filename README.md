# ROSRider ReadMe

## Riders Arena Aşama 1 – Ön Eleme Hazırlık Projesi

### Riders Arena 1. Aşama’ya hoş geldiniz!

Bu döküman ile size,

*  Bir projeyi genel olarak nasıl kullanacağınızı,
*  Robotumuz RosRider ile tanışmanızı,
*  ROSRider kütüphanesini nasıl kullanacağınızı,
*  Hazırlık Pistinde algoritmaları nasıl test edeceğinizi

göstermeye çalışacağız.

Bu yazıyı sonuna kadar okumadan önce, aşağıdaki kısa videoyu izlemenizi tavsiye ediyoruz:

[Riders’ta Robotunu Nasıl Kodlarsın?](https://www.youtube.com/watch?v=nIQtvTus8so)

Yukarıdaki adımları gerçekleştirmeniz ve Ön Eleme Yarışmasına hazır olmanız için tam 7 gününüz var. Hazırsanız hadi başlayalım!

Açılan ilk profil sayfasında sağ üst köşedeki “Riders Arena” tuşuna ya da ortadaki yeşil renkli linke basarak Riders Arena sayfasına girmelisiniz.

<br/> </br>

<img src="images/1welcome.png" width="1000">

<br/> </br>

Yarışmayla ilgili projelere bu sayfadan erişebiliyoruz. **Ön Eleme Yarışması Hazırlık Projesi’ne** yeşil renkli Go To Project tuşuyla gidebiliriz. 

<br/> </br>

<img src="images/2kubilay.png" width="1000">

<br/> </br>

Açılan yarışma hazırlık projesini kendi profilinize kopyalamanız için sağ üstteki yeşil renkli fork butonuna basmanız gerekir. 
Bu sayede projeye daha sonra kendi panelinizden (dashboard) direk ulaşabileceksiniz. 
Örnek projede ilerleyen günlerde değişiklikler olabilir. 
Böyle bir durumda sizi bilgilendireceğiz ve yeniden bu şekilde forklayarak en güncel projeyi kullanabileceksiniz.

<br/> </br>

<img src="images/6riders_stage.png" width="1000">

<br/> </br>

Fork butonuna bastığınız zaman karşınıza küçük bir pop-up ekranı gelecektir.

<br/> </br>

<img src="images/create_project.png" width="1000">

<br/> </br>

Bu küçük pop-up ekranında karşınıza proje ismi, proje sloganı ve proje takımı otomatik olarak doldurulmuş bir şekilde gelecektir. Burada alt menüden “Private” seçeneğini seçip yeşil renkteki “Create Project” butonuna basarak devam etmelisiniz.

Bunu gerçekleştirdikten sonra proje kendi panelinize (dashboard) kopyalanacak ve açılan proje sayfası sadece sizin düzenleyebileceğiniz, sizden başka kimsenin göremediği bir proje olacak. 

Bu proje sayfasında bir önceki proje sayfasına göre bir farklılık var. Bu sayfada “Fork” butonunun sol tarafında yeşil renkte “</>Edit” butonu karşınıza çıkacak. 

Projede "</>Edit" tuşuna bastığınızda varsa daha önceki versiyonlardan birini seçmenizi ya da en son kaldığınız yerden devam edip etmeyeceğinizi soracaktır. 
**Projeyi ilk defa forkladıktan sonra lütfen en son versiyonu seçerek devam edin.** 

<br/> </br>

<img src="images/4kubilay.png" width="1000">

<br/> </br>

Kısa bir yüklemeden sonra karşınıza bir editör (yazılım geliştirme ortamı) açılacaktır. Bu editöre RIDE diyoruz. RIDE’ı açtığınız anda aşağıdaki gibi bir editör ekranı sizi karşılayacaktır.

<br/> </br>

![](images/openning_screen.png)

<br/> </br>

Editörün sol sütununda yer alan Proje (Explorer) bölümü halihazırda açıktır. Burada mevcut Çalışma Alanı (Workspace) dizinlerini görebilir ve yenilerini oluşturabilirsiniz. 

Çalışma Alanı bir catkin dizinidir. Src’nin altında yeni yazılım paketleri yaratabilir ya da **Edit Project Configuration**’ı kullanarak Github’tan diğer paketleri yükleyebilirsiniz. 

Proje bölümünün üzerinde standart VS Code eylemleri bulunur. Buradan yeni dosyalar, dizinler oluşturabilir ya da terminal açabilirsiniz.

<br/> </br>

![](images/vs_code_actions.png)

<br/> </br>

Bölümün altında Riders logosu ile Riders’a özel olan bir menü açılır. Bu menüden simulasyonu başlatıp, durdurabilir, resetleyebilirsiniz. Ayrıca projeyi derleyebilir, yayınlayabilir, proje konfigürasyonunu değiştirebilir ve son olarak da editörü durdurabilirsiniz.

Bu bölümdeki Nodes sekmesinde kodlama yapabileceğiniz hazır dosyaları bulabilirsiniz. **Publish Project** tuşu src dizinindeki dosyaları yeni bir versiyon olarak kayıt eder. Editörü yeniden açarken bu versiyonu seçerek çalışmalarımızı devam ettirebiliriz. 

<br/> </br>

![](images/riders_actions.png)

<br/> </br>

Bazı notlar:

*  Eğer çalıştığınız projeyi başka bir projeden Fork tuşu ile aldıysanız, **Build Project** tuşuna bastığınızda Riders bölümünün en üstündeki Start tuşu ortaya çıkacaktır.
*  Eğer çalıştığınız projeyi siz kendiniz oluşturduysanız bu durumda konfigürasyon ayarlarını da yapmalısınız. Bunun için **Edit Project Configuration** tuşunu kullanmak gerekmektedir. Ancak bu yarışma için böyle bir şey yapmanıza gerek olmadığı için bu konu daha sonra anlatılacaktır.

**Start** tuşuna basıldığında, **Edit Project Configuration** altında tanımlanmış konfigürasyon ile simulasyon çalışacaktır. Eğer paketler ile ilgili bir hata yoksa simulasyon düzgün bir şekilde çalışacaktır ve görseldeki gibi bir simulasyon ekranı gözükecektir.

*  **View Simulator** tuşu eğer bir şekilde simulator ekranı kapandıysa yeniden bunu açar.
*  **Reset** tuşu simulasyonu resetler ve tüm fonksiyonları yeniden başlatır.
*  **Stop tuşu** simulasyonu durdurur.

<br/> </br>

![](images/simulation.png)

<br/> </br>

Ayrıca, Simulasyon’la birlikte yine sol bölmede Sensors sekmesi de açılacaktır.

<br/> </br>

![](images/running_actions.png)

<br/> </br>

Sensors sekmesi altında kamera görsellerine erişebilirsiniz.


**Programlamaya başlamadan önce robotumuzu biraz daha yakından tanıyalım:**

<br/> </br>

<img src="images/1rosrider.png" width="800">

<br/> </br>

Robotumuz ROSRider hakkında yarışma web sitesinin kurallar bölümünde daha önce bazı bilgilendirmelerde bulunmuştuk. Buradaki amacımız ise robotun teknik parametreleri hakkında biraz daha detaylı bilgi vermek. 

**Kamera sensörü**


Yukarıdaki resimde görüldüğü üzere robotumuzun üzerinde bir kamera sensörü bulunuyor. Bu kamera sensörü 32x1 RGB bir satır kamera. Öncelikle bu kameranın geometrik özelliklerini tanıyalım.

<br/> </br>

<img src="images/2rosrider_up.png" width="400">

<br/> </br>

ROSRider Kamera Sensörü robotun merkez noktasından 0.38 metre uzaklıktaki objeleri görebilir. Kamera yere doğru açılı yerleştirildiği için yerdeki çizgiyi bu kamera ile rahatlıkla takip edebilirsiniz. Kameranın uzağı görmesi çizgiyi takip etmek konusunda size bir kolaylık sağlayacaktır. Kamera sensörünün yatayda 0.36 m’lik bir görüş mesafesi bulunmaktadır. Çizginin kalınlığı aşağıdaki resimde de görüleceği üzere yaklaşık 6 pixellik bir alan kaplamaktadır. Buradan çizgi kalınlığını da hesaplayabilirsiniz. 

<br/> </br>

<img src="images/3piksel.png" width="600">

<br/> </br>

Sensörün aldığı görüntüye RIDE Editörü üzerinden kolaylıkla ulaşabilirsiniz:

<br/> </br>

![](images/4snapshot_camera_image.png)

<br/> </br>

Sensors bölümünden sarı ile işaretlenen yerdeki ikonlara tıklayarak snapshot ya da sürekli görüntü (stream) alabilirsiniz.
Kameradan alınan görüntünün kolay işlenebilmesi amacıyla, pixellerdeki parlaklık verilerini bir değişkene kaydeden bir fonksiyonumuz mevcut. Bu sayede kameradan gelen veriyi bir dizi (array) halinde sizinle paylaşıyoruz. 
self.sensor_row
32 elemanlı olan bu dizi her bir pikselin parlaklık değerlerini tutmaktadır. 
Bu dizi üzerinden her bir pikseldeki parlaklık değerini okuyabilir ve çizginin nerede olduğuna karar verebilirsiniz.

## Robotun Hareketi

Robotu hareket ettirebilmeniz için iki komut mevcuttur. Bu komutlar ile robot doğrusal (lineer) ya da açısal hareket edebilmektedir.

Aşağıdaki resimde de gösterildiği üzere robotunuzu 


self.robot.move() ve self.robot.rotate() komutları üzerinden doğrusal ya da açısal yönde hareket ettirebilirsiniz.

<br/> </br>

<img src="images/robot_move.png" width="800">

<br/> </br>

Yukarıda anlatıldığı gibi Nodes bölümünden Node:line_follower’ı seçtiğiniz zaman karşınıza line_follower.py dosyası açılacak. Yarışma için kullanacağınız algoritmayı bu dosya içerisinde geliştirmeniz gerekiyor. Şimdi de bu dosyayı biraz inceleyelim:

<br/> </br>

<img src="images/5library.png" width="800">

<br/> </br>

İlk bölümde kütüphaneler tanımlanıyor. Bir Python programlayıcısı olarak bunu zaten biliyorsunuz. Buradaki eklenmiş olan kütüphaneler yarışma için gerekli kütüphaneler. Bunlar haricinde başka kütüphanelerden faydalanmak mümkün. Örnek olarak algoritmanı geliştirirken:

<br/> </br>

<img src="images/6library_example.png" width="300">

<br/> </br>

gibi kütüphanelerden faydalanabilirsin. 

Şimdi gelelim robotumuzun class (sınıf) tanımlamasına;

<br/> </br>

<img src="images/7line_follwer.png" width="800">

<br/> </br>

Yukarıdaki resimde görüldüğü üzere, robotumuzun class tanımlaması class LineFollower

Her Python class yapısında olduğu gibi öncelikle def **__init__(self)** fonksiyonu içerisinde gerekli
tanımlamalar mevcut. Örnek olarak:

<br/> </br>

<img src="images/8line_follwer_example_line.png" width="800">

<br/> </br>

self.sensor_row = [] fonksiyonu robotun önündeki kamera sensorü verilerini dizide tutuyor. Hatırlarsanız “Kamera Sensörü” bölümünde bu diziden bahsetmiştik.

Temel olarak bu fonksiyon altında global değişkenlerinizi tanımlayabilirsiniz. Sensörün görüş mesafesini 0.38 metre olarak yukarıda belirtmiştik. Bu değeri aşağıdaki şekilde tanımlayarak, yazacağımız diğer fonksiyonlardan da çağırabiliriz.

<br/> </br>

<img src="images/9_codes.png" width="800">

<br/> </br>

Bu fonksiyonun içerisinde halihazırda yazılı olan kodun tamamı sistemin düzgün çalışması için gereklidir. Bu yüzden halihazırdaki kodu silmemeniz simülasyonun istediğiniz gibi çalışması bakımından önemli. 

**line_follower.py** dosyasındaki bir sonraki fonksiyon algoritmanızın yer alacağı **process_image_and_steer** fonksiyonu. Bu fonksiyon içerisinde yine halihazırda yazılı olan kod simülasyonun düzgün çalışması için gereklidir.

<br/> </br>

<img src="images/10_codes.png" width="800">

<br/> </br>

Sizden istenen yukarıdaki sarı dikdörtgenler ile işaretlenmiş kısımların altında çizgiyi bulma algoritmanızı ve robotun çizgiyi hızlı bir şekilde takip etmesini sağlayacak hareket algoritmanızı geliştirmenizdir.

Örnek olarak:

Aşağıdaki kod robotun bir daire çizmesini sağlayacaktır. 

<br/> </br>

<img src="images/11_codes.png" width="800">

<br/> </br>

Bu fonksiyonların haricinde farklı fonksiyonlar mevcut olduğu gibi, kendi fonksiyonlarınızı da oluşturabilirsiniz. Şimdi projemizdeki robotun fonksiyonlarına ve değişkenlerine bir göz atalım:

* **self.sensor_row[]**  
Kamera sensöründen gelen verinin parlaklık değerlerini bir dizide tutmaktadır.  
Array Size and Type:: 32 , integer


* **self.robot.move(linear_vel)**  
Robotun doğrusal yönde linear_vel değeri büyüklüğünde bir hızla ileri doğru hareket etmesini sağlar.  
linear_vel: float

* **self.robot.rotate(angular_vel)**  
Robotun açısal yönde angular_vel değeri büyüklüğünde bir hızla dönme hareketi yapmasını sağlar.  
angular_vel: float

* **self.robot.x**  
Robotun uzaydaki x konum bilgisini tutan değişkendir.  
x: float

* **self.robot.y**  
Robotun uzaydaki y konum bilgisini tutan değişkendir.  
y: float

* **self.robot.yaw**  
Robotun yere göre açısal konum bilgisini tutan değişkendir.  
yaw: float

* **self.robot.vel_x**  
Robotun doğrusal yöndeki hız bilgisini tutan değişkendir.  
x: float

* **self.robot.vel_z**  
Robotun açısal hız bilgisini tutan değişkendir.  
z: float

Yukarıdaki fonksiyon ve değişkenleri kullanarak algoritmanızı geliştirebilirsiniz. Şimdi algoritmanızı geliştirirken değişkenleri nasıl izleyeceğinize ve nasıl hata ayıklayacağınıza bakalım.
Değişken İzleme ve Hata Ayıklama
Geliştirdiğiniz algoritmadaki değişkenleri izlemek ve hatalarınızı ayıklamak için aşağıdaki yöntemi kullanmanızı tavsiye ediyoruz:

## Değişken İzleme:

Değişken izlemek için iki farklı yöntemimiz mevcut. İlk yöntemimiz ile lacivert bölümdeki yarışma metriklerini robotun metriklerine
dönüştürebiliriz. Bunun için aşağıdaki video eşliğinde ufak bir değişiklik yapmamız gerekiyor.

<br/> </br>

![](images/simple_observer.gif)

<br/> </br>

videoda görüldüğü üzere **Edit Project Configuration** bölümüne tıkladığımızda **adaptive_evaluator** yazan kısımlara **simple_observer** yazarak
yarışma metrikleri yerine robotun metriklerini gösterebiliriz. Buradaki metrikleri sırayla tanımlayalım:

**err:** robotun bir hatası olup olmadıgını veren bir değişkendir

**th:** robotun anlık olarak açısal konum bilgisini veren bir değişkendir

**x:** robotun anlık olarak doğrusal hızını veren bir değişkendir

**z:** robotun anlık olarak açısal hızını veren bir değişkendir

Algoritmanızı geliştirirken bu değişkenleri görüntüleyerek robotun değişkenlerini izleyebilirsiniz. 

Bir diğer yöntem ise robotun metriklerinde olmayan kendi yarattığınız bir değişkeni izlemek.

Yukarida self.sensor_row dizisinden bahsetmiştik. 32 elemanlı bu dizi; kamera sensöründen gelen parlaklık değerlerini tutuyordu. 
Şimdi bu sensörün tam ortasındaki pixelin robot hareket ederken ne ölçtüğüne aşağıda bakalım. Bunu gerçekleştirmek için print() fonksiyonunu kullanıyoruz. Sonrasında aşağıdaki videoda gösterildiği gibi değişkeni izlemeye başlayabiliriz. 

<br/> </br>

![](images/ezgif.com-video-to-gif.gif)

<br/> </br>

Not: Bu yöntem nadir de olsa hatalı sonuç verebilir, çünkü aynı program iki kere çalışıyor durumda. 

Bunu düzeltmek için:

Yine yukarıda olduğu gibi **Edit Project Configuration** a tıklayarak konfigürasyon dosyamızı açmalıyız. Bu sefer yapacağımız:

```yaml
    line_follower:
        node: line_follower.py
        package: competition
```

bölümünü tamamen silmemiz. Çünkü değişkenleri izlemek için videoda gösterildiği gibi o dosyayı "Run File in Python Terminal" yapacağız.

Bu dosyayı bir önceki videoda gösterildiği gibi kaydedip, ekranın sağ alt köşesinde Configuration Saved yazdığından emin olun.

Sonrasında yukarıdaki videoda ve açıklamada anlatıldığı gibi print() fonksiyonu ile birlikte ilerleyerek değişkenleri izleyebilirsiniz.


## Hata Ayıklama:

Benzer şekilde hatalarımızı da ayıklayabiliriz (debug). Ancak burada hataları ikiye ayırmak gerekiyor:

1.  Syntax Hataları
2.  Fonksiyonel Hatalar

Öncelikle Syntax Hataları’ndan başlayalım
Eğer kodunuzda bir syntax hatası var ise, start veya reset ettiğinizde bir hata ile karşılaşacaksınız. Aşağıda gösterildiği gibi bir syntax hatası yerleştirelim. Yerleştirdiğimiz syntax hatası sonucu program aşağıda görüldüğü üzere bir hata veriyor. Bu hatanın tam olarak nerede olduğunu anlamak için videodaki gibi logs bölümünden line_follower.log dosyasına tıklayalım. Açılan pencere bize hatanın hangi satırda ve ne hatası olduğunu gösterecektir.

<br/> </br>

![](images/syntax_error.gif)

<br/> </br>

Fonksiyonel hatalar simülasyonun çalışmasını engellemiyor olsa da robotun düzgün çalışmasını engelleyecektir. Bu yüzden bu hataların tespit edilmesi gerekir. Nasıl tespit edeceğimizi bir örnek ile açıklayalım:

Yukarıda bahsettiğimiz üzere kamera sensöründen gelen verileri

self.sensor_row 

isimli 32 elemanlı bir dizi (array) değişkeninde tutuluyordu.

Kodumuza şöyle bir satır ekleyelim:

pixel_value = self.sensor_row[36]

yukaridaki kod bir hata oluşturacaktır. Çünkü sensor_row dizisinin 36 elemanı yoktur.

Aşağıdaki videoda gösterildiği üzere hatayı tespit edebiliriz. Değişken izlemedekine benzer olan süreçte hızlıca fonksiyonel hata ve hataları bulabiliriz.

<br/> </br>

![](images/functional_error.gif)

<br/> </br>
